Dit project(je) dient om gegenereerde swagger documentatie (in json formaat) om te vormen naar asciidoc en pdf.
Pas zo nodig de url aan in SwaggerToAsciiDocConverter, en start de applicatie.
Het resultaat komt terecht in
* build/api-docs.adoc
* build/api-docs.pdf