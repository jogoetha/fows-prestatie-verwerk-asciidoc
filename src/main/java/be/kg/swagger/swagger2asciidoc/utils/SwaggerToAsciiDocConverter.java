package be.kg.swagger.swagger2asciidoc.utils;

import io.github.swagger2markup.Swagger2MarkupConverter;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Asciidoctor.Factory;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static org.asciidoctor.OptionsBuilder.options;

public class SwaggerToAsciiDocConverter {


    public static void main(String[] args) throws MalformedURLException {
        Path localSwaggerJsonFile = Paths.get("src/main/resources/swagger/swagger.json");
        URL swaggerJsonUrl = new URL("http://localhost:8083/v2/api-docs?group=OpvangPrestaties");
        Swagger2MarkupConverter.from(swaggerJsonUrl)
                .build()
                .toFile(Paths.get("build/api-docs"));
        Map<String, Object> options = options()
                .inPlace(true)
                .backend("pdf")
                .asMap();
        Asciidoctor asciidoctor = Factory.create();
        String outfile = asciidoctor.convertFile(Paths.get("build/api-docs.adoc").toFile(), options);
    }



}
